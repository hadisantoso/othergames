<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');
Route::get('/destination/{slug}','HomeController@destinationDetail')->name('destination');

Route::get('/logout','Auth\AuthController@logout')->name('logout');

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('/reveal/{slug}', 'HomeController@shareFacebook')->name('sharefacebook');

Route::get('/user','HomeController@getUser');

Route::post('/profile/submit/{slug}', 'Auth\AuthController@submitProfile')->name('profile.store');

Route::post('/upload/shareimage','HomeController@uploadCropImageFB')->name('share.fb.upload');

Route::get('/kebijakan-privasi','HomeController@getKebijakanPrivasi')->name('kebijakan.privasi');
Route::get('/syarat-ketentuan','HomeController@getSyaratKetentuan')->name('syarat.ketentuan');



View::composer(
    ['layouts.menu'],
    'App\Http\ViewComposer\DestinationComposer'
);
