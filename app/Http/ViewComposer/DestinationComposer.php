<?php

namespace App\Http\ViewComposer;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use App\Models\Destination;

class DestinationComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $destinations;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        $destinations = Destination::all();
        $this->destinations = $destinations;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('menu_destinations',$this->destinations);
    }
}
