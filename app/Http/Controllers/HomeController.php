<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Destination;
use App\Models\HomeVideoSlider;
use App\Models\Syaratketentuan;
use App\Models\Kebijakanprivasi;
use Image;

use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    //
    public function index(Request $request){
      // dd(Auth::guard('customer')->user());
      $destinations = Destination::where('is_featured',1)->get();
      $videos = HomeVideoSlider::all();
      return view('index',['destinations' => $destinations, 'videos' => $videos]);
    }

    public function destinationDetail($slug){
        $destination = Destination::where('slug',$slug)->first();
        $destinations = Destination::all();
        $isShared = '0';
        if(Auth::guard('customer')->check()){
          $user = Auth::guard('customer')->user();
          if ($user->destinations->contains($destination->id)) {
            $isShared= '1';
          }
          return view('other-destination',['destination' => $destination, 'destinations'=> $destinations,'user'=>Auth::guard('customer')->user(),'isShared'=>$isShared]);
        }
        return view('other-destination',['destination' => $destination,'destinations'=> $destinations, 'isShared'=>$isShared]);
    }

    public function shareFacebook($slug){
      $user = Auth::guard('customer')->user();
      $destination = Destination::where('slug',$slug)->first();
      $destinations = Destination::all();
      return view('other-destination',['destination' => $destination,'user'=>Auth::guard('customer')->user(),'destinations'=> $destinations,'isShared'=>'1']);
    }

    public function getUser(){
      return Auth::guard('customer')->user();
    }

    public function uploadCropImageFB(Request $request){
      $destination_path = "image_share_tmp";
      $disk = "uploads";
      $image = Image::make(file_get_contents($request->image));
//      $image->resize(1200, 688);
      $filename = uniqid('img_share').'.jpg';
      \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
      return response()->json(['path'=>$destination_path.'/'.$filename]);
    }

    public function getKebijakanPrivasi(){
      $data = Kebijakanprivasi::first();
      return view('kebijakan-privasi',['data'=>$data]);
    }

    public function getSyaratKetentuan(){
      $data = Syaratketentuan::first();
      return view('syarat-ketentuan',['data'=>$data]);
    }
}
