<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DestinationRequest as StoreRequest;
use App\Http\Requests\DestinationRequest as UpdateRequest;

/**
 * Class DestinationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DestinationCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Destination');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/destination');
        $this->crud->setEntityNameStrings('destination', 'destinations');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();
        $this->crud->removeField('slug');
        $this->crud->addField([
            'name' => 'thumbnail',
            'label' => "Thumbnail",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 440/288,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'icon_thumbnail1',
            'label' => "Thumbnail Icon 1",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 150/150,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'icon_thumbnail2',
            'label' => "Thumbnail Icon 2",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 150/150,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'featured_image',
            'label' => "Featured Image",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 1200/522,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'featured_image_mobile',
            'label' => "Featured Image Mobile",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 480/522,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'image1',
            'label' => "Image 1",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 471/324,
            'prefix' => 'uploads/',
        ]);
        $this->crud->addField([
            'name' => 'image2',
            'label' => "Image 2",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 587/351,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'icon_image_detail',
            'label' => "Image Icon",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 150/150,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'thumbnail_background',
            'label' => "Thumbnail Background",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 441/288,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'info_icon',
            'label' => "Info Icon",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 217/59,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'popup_bg',
            'label' => "Popup BG",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 1048/544,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([
            'name' => 'popup_bgig',
            'label' => "Popup BGIG",
            'type' => "image",
            'upload' => true,
            'crop' => true,
            'aspect_ratio' => 640/640,
            'prefix' => 'uploads/',
        ]);

        $this->crud->addField([   // Checkbox
            'name' => 'is_featured',
            'label' => 'Featured',
            'type' => 'checkbox'
        ]);
        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // add asterisk for fields that are required in DestinationRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '=', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $slug = str_slug($request->title, '-');
        $request->request->add(['slug' => $slug]);
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $slug = str_slug($request->title, '-');
        $request->request->add(['slug' => $slug]);
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
