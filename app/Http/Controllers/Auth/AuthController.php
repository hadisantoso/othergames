<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;
use App\Models\Destination;



use Socialite;

class AuthController extends Controller
{
  public function redirectToProvider(Request $request,$provider)
   {
     // dd(URL::previous());
       $request->session()->put('backUrl',  URL::previous());
       return Socialite::driver($provider)->redirect();
    }

   /**
    * Obtain the user information from provider.  Check if the user already exists in our
    * database by looking up their provider_id in the database.
    * If the user exists, log them in. Otherwise, create a new user then log them in. After that
    * redirect them to the authenticated users homepage.
    *
    * @return Response
    */
   public function handleProviderCallback(Request $request,$provider)
   {
       $user = Socialite::driver($provider)->stateless()->user();
       // dd($user);
       $authUser = $this->findOrCreateUser($user, $provider);
       Auth::guard('customer')->login($authUser);
       if ($request->session()->has('backUrl')) {
         $uri_path = $request->session()->get('backUrl');
         $uri_parts = explode('/', $uri_path);
         $uri_tail = end($uri_parts);
         return redirect()->route('destination',['user'=>$authUser,'slug'=>$uri_tail])->with('showpop','1');
       }
       return redirect('/');
   }

   /**
    * If a user has registered before using social auth, return the user
    * else, create a new user object.
    * @param  $user Socialite user object
    * @param $provider Social auth provider
    * @return  User
    */
   public function findOrCreateUser($user, $provider)
   {
       $authUser = Customer::where('provider_id', $user->id)->first();
       if ($authUser) {
           return $authUser;
       }

       else{
           $destination_path = "images";
           $disk = "uploads";
           $image = Image::make($user->avatar.'&heigth=1200&width=1200');
           $image->resize(400, 400);
           $filename = uniqid('img_').'.jpg';
           \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
           $path= $destination_path.'/'.$filename;
           $data = Customer::create([
               'name'     => $user->name,
               'email'    => !empty($user->email)? $user->email : '' ,
               'provider' => $provider,
               'provider_id' => $user->id,
               'image' => $path,
               'token' => $user->token,
           ]);
           return $data;
       }
   }

   public function logout()
   {
     Auth::guard('customer')->logout();
     return redirect('/');
   }

   public function submitProfile(Request $request, $slug){
        $destination_path = "images";
        $disk = "uploads";
        $customer = Auth::guard('customer')->user();
        $destination = Destination::where('slug',$slug)->first();
        if (! $customer->destinations->contains($destination->id)) {
            $customer->destinations()->save($destination);
        }
        if($request->has('photo_profile')){
          $file = $request->file('photo_profile')->getRealPath();
          $image = Image::make($file);
          $image->resize(400, 400);
          $filename = uniqid('img_').'.jpg';
          \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
          $customer->image = $destination_path.'/'.$filename;
        }
        $customer->name = $request->name;
        $customer->country = $request->country;
        $customer->save();
        $destinations = Destination::all();
        $request->session()->flash('showpopshare','1');
        return view('other-destination',['destination' => $destination,'user'=>Auth::guard('customer')->user(),'destinations'=> $destinations,'isShared'=>'1']);
   }
}
