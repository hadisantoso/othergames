$(function () {
    $('a[href*="#"].smooth').on('click', function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'linear');
    });

    $("#menu-toggle-desktop,#menu-toggle-mobile").on('click', function () {
        $("#wrapper").toggleClass("toggled");
    });

    $("#menu-toggle-close").on('click', function () {
        $("#wrapper").toggleClass("toggled");
    });

    $('.video-custom').parent().click(function () {
        if ($(this).children(".video-custom").get(0).paused) {
            $(this).children(".video-custom").get(0).play();
            $(this).children(".playpause").fadeOut();
        } else {
            $(this).children(".video-custom").get(0).pause();
            $(this).children(".playpause").fadeIn();
        }
    });

    $('#btn-more-destination').on('click', function () {
        $('#destination .hidden').show();
        $(this).hide();
    });
    $('#destination a figure').click(function (e) {
        $(this).removeClass('bg-overlay');
    });

    if ($('.car-images').length > 0) {
        $('.car-images').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            arrows: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
});