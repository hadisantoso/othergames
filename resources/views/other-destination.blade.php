@extends('layouts.layout')
@section('content')
    <div id="other-destination-content">
        <div class="banner-destination">
            <img src="{{ url('uploads/'.$destination->featured_image) }}" class="img-fluid d-none d-sm-block" alt="">
            <img src="{{url('uploads/'.$destination->featured_image_mobile)}}" class="img-fluid d-block d-sm-none" alt="">
        </div>

        <div class="container">
            <div class="row margin-title-detail margin-top-120">
                <div class="col-md-6">
                    <h3 class="title-sec2">{{$destination->title_detail}}</h3>
                    <div class="bottom-rainbow">
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="img-title">
                        <img src="{{ url('uploads/'.$destination->icon_image_detail) }}" class="img-animal-burung"
                             alt="">
                        <div class="title-bg"
                             style="background: {{ $destination->mascot_color }}">{{$destination->title_detail2}}</div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <img src="{{ url('uploads/'.$destination->image1) }}"
                         class="img-fluid img-destination margin-top-110" alt="">
                </div>
                <div class="col-md-6">
                    <div class="short-content content-right text-blue">
                        {!! $destination->image1_desc !!}
                        <div class="mobile"><br>{!! $destination->image2_desc !!}</div>
                    </div>
                </div>
            </div>

            <div class="clear-100 desktop"></div>

        </div>

        <div>
            <div class="row">
                <div class="col-md-12 text-center mobile">
                    <img src="{{ url('uploads/'.$destination->image1) }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 content-left-bg">
                    <div class="bg-section">
                        <div class="text-bg-left-sec">
                            {!! $destination->image2_desc !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 img-right-bg order-first order-lg-2"
                     style="background: url({{ url('uploads/'.$destination->image2) }}) no-repeat bottom; background-size: cover;">
                    <div class=" bg-section-img"></div>
                </div>
            </div>
            <div class="bg-grey" style="background:#f1f2f1; padding: 15px 0 30px 0;">
                <div class="container no-margin-top">
                    <div class="row">
                        
                        <div class="col-md-12">
                            @if(Auth::guard('customer')->check())
                                    <br><br><span class="heading-get">Getting There</span><a id="modalRegister" data-toggle="modal"
                                           data-target="#registerModal" href="javascript:void(0)"><img
                                    src={{asset('assets/images/get-travel-promo.png')}} alt="" class="buy-ticket"><img
                                    src={{asset('assets/images/logo-blibli.png')}} alt="" class="buy-ticket"></a>
                            @else
                                    <br><br><span class="heading-get">Getting There</span><a href="{{route('oauth',['provider'=>'facebook','prevPage'=>$destination->slug])}}"><img
                                    src={{asset('assets/images/get-travel-promo.png')}} alt="" class="buy-ticket"><img
                                    src={{asset('assets/images/logo-blibli.png')}} alt="" class="buy-ticket"></a>
                            @endif
                            
                        </div>
                        
                    </div>
                </div>

                <div class="container margin-top-30">
                    <div class="row">
                        <div class="col-md-12 center-mobile">
                            <img src="{{ url('uploads/'.$destination->info_icon) }}" class="animated fadeIn delay-1s"
                                 alt="">
                            <div class="text-blue animated slideInUp margin-top-30 text-left">
                                {!! $destination->info_desc !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row margin-top-90 no-margin-top">
                        <div class="col-md-12 center-mobile">
                            <span class="heading-get">For more information:</span>
                            <div class="text-blue animated">
                                <a target="_blank" href="http://{{ $destination->info_link }}"><p
                                            class="text-purple">Contact Us</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                @if(Auth::guard('customer')->check())
                <a id="modalRegister" data-toggle="modal" data-target="#registerModal" href="javascript:void(0)"><img
                            src={{asset('assets/images/get-travel-promo.png')}} alt="" class="buy-ticket-mobile"><br><img
                                    src={{asset('assets/images/logo-blibli.png')}} alt="" class="buy-ticket-mobile"></a>
                @else
                <a href="{{route('oauth',['provider'=>'facebook','prevPage'=>$destination->slug])}}"><img
                            src={{asset('assets/images/get-travel-promo.png')}} alt="" class="buy-ticket-mobile"><br><img
                                    src={{asset('assets/images/logo-blibli.png')}} alt="" class="buy-ticket-mobile"></a>
                @endif
            </div>
            <div class="container">
                
                <br><br>
            </div>

            <div class="content margin-top-130">
                <div class="car-images">
                    @foreach ($destinations as $destination_other)
                        <a href="{{route('destination',$destination_other->slug)}}">
                            <figure class="imghvr-slide-up width-full bg-overlay">
                                <img src="{{ url('uploads/'.$destination_other->thumbnail) }}"
                                     class="img-fluid width-full">
                                <figcaption>
                                    <div>
                                        <h4 class="upppercase">{{$destination_other->thumbnail_title}}</h4><br>
                                        <p>{{$destination_other->thumbnail_desc}}</p><br>
                                        <img src={{asset("assets/images/read-more.png")}} alt="" style="width: 130px;">
                                    </div>
                                </figcaption>
                            </figure>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-custom modal-dialog-centered" role="document">
            <div class="modal-content"
                 style="background: url({{ url('uploads/'.$destination->popup_bg) }}) no-repeat; background-size: cover; width: 100%;">

                <div class="modal-body modal-overlay">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="d-none d-md-block d-lg-none"><img
                                    src={{asset('assets/images/btn-close-big.png')}} alt=""></span>
                        <span aria-hidden="true" class="d-md-none d-lg-block"><img
                                    src={{asset('assets/images/close.png')}} alt=""></span>
                    </button>
                    <form method="post" action="{{ route('profile.store', $destination->slug)}}" id="form_profile"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="register-content">
                                        <h1 class="heading-pop margin-top-160">
                                            {{ $destination->popup_title }}
                                        </h1>
                                        <div class="bg-modal-text-register">
                                            {!! $destination->popup_desc !!}
                                        </div>
                                        <img src="{{asset('assets/images/button-submit-big.png')}}" alt=""
                                             class="btn-submit"
                                             onclick="submitProfile();">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="area-profile">
                                        <div class="row">
                                            <div class="col-md-7 col-6">
                                                <img src="{{URL::asset('assets/images/logo-popup.png')}}"
                                                     class="img-fluid logo-modal" alt="">
                                            </div>
                                            <div class="col-md-5 col-6">
                                                <div>
                                                    @if(isset($user->image))
                                                        @if (strpos($user->image, 'facebook') !== false)
                                                            <label class="area-photo photo-fb" id="label-area-photo"
                                                                   for="photoProfile"
                                                                   style="background: url('{{$user->image}}') no-repeat"></label>
                                                        @else
                                                            <label class="area-photo photo-fb" id="label-area-photo"
                                                                   for="photoProfile"
                                                                   style="background: url('{{url('uploads/'.$user->image)}}') no-repeat"></label>
                                                        @endif
                                                    @else
                                                        <label class="area-photo" id="label-area-photo"
                                                               for="photoProfile">Upload Your Photo
                                                            Here!</label>
                                                    @endif
                                                    <input type="file" id="photoProfile" onchange="readURL(this);"
                                                           name="photo_profile">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <div class="form-athlete">
                                                    <h4 class="text-viola text-bold">OTHER ATHLETE</h4>
                                                    <input type="text" class="form-control your-name"
                                                           placeholder="Insert Your Name"
                                                           value="{{isset($user->name)?$user->name:""}}" name="name">
                                                    <input type="text" class="form-control your-name"
                                                           placeholder="Your Country"
                                                           value="{{isset($user->country)?$user->country:"Indonesia"}}"
                                                           name="country">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row nopadding"
                                             style="background: {{ $destination->popup_bgcolor }};">
                                            <div class="col-md-6 col-6 text-center">
                                                <div class="popup-icon-logo">
                                                    <img src="{{ url('uploads/'.$destination->icon_thumbnail1) }}"
                                                         alt="">
                                                    <p>{{ $destination->category }}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-6">
                                                <div class="place"
                                                     style="background: {{ $destination->popup_btncolor }};">
                                                    PLACE: <br>{!! $destination->popup_place !!}
                                                </div>
                                                <div class="sitting"
                                                     style="background: {{ $destination->popup_btncolor }};">
                                                    SITTING: <br>{!! $destination->popup_sitting !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-margin-top submit-popup">
                                        <img src="{{asset('assets/images/btn-submit.png')}}" alt=""
                                             class="btn-submit d-block d-sm-none margin-0"
                                             onclick="submitProfile();">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="after-register" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-custom modal-dialog-centered" role="document">
            <div class="modal-content" id="full-modal-profile"
                 style="background: url({{ url('uploads/'.$destination->popup_bg) }}) no-repeat; background-size: cover; width: 100%;">

                <div class="modal-body">
                    <button type="button" id="after-close-button" class="close" data-dismiss="modal" aria-label="Close"
                            data-html2canvas-ignore="true">
                        <span aria-hidden="true" class="d-none d-sm-block"><img
                                    src={{asset("assets/images/btn-close-big.png")}} alt=""></span>
                        <span aria-hidden="true" class="d-block d-sm-none"><img
                                    src={{asset("assets/images/close.png")}} alt=""></span>
                    </button>
                    <div class="bg-modal-text congratulations text-center d-none d-sm-block d-md-none d-block d-sm-none"
                         data-html2canvas-ignore="true">
                        <h3>Congratulations!</h3>
                        <div>
                            You are one of the first batch of
                            other-athletes in Other Games.
                        </div>
                        <div>
                            Share your participation and let everyone
                            know you’re representing your country in
                            Other Games 2018.
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bg-modal-text congratulations d-none d-xl-block"
                                     data-html2canvas-ignore="true">
                                    <h3>Congratulations!</h3>
                                    <div>
                                        You are ready for the actions of leisure in Other Games. Share your tag with the button below and let everyone see your participation.
                                    </div>
                                    <br>
                                    <div class="promo-code">
                                        YOUR PROMO CODE:<br>
                                        <h2>BLIBLITRAVEL-10</h2>
                                        *Can be used by Blibli.com verified member. 10% discount for flight and hotel with minimum purchase of Rp 1.000.000. Voucher code can be used until 30 November 2018."
                                    </div>
                                </div>

                                <div class="sosmed-icon d-none d-md-block" id='sosmed-share-desktop' data-html2canvas-ignore="true">
                                    <a href="javascript:void(0)" onclick="shareFB()"><img
                                                src={{asset("assets/images/fb.png")}} alt=""></a>
                                    <!-- <a href="https://twitter.com/intent/tweet?text=https://www.othergames2018.com"><img
                                                src={{asset("assets/images/tw.png")}} alt=""></a>
                                    <a href="javascript:void(0)" id="download-instagram" class="download-instagram"><img
                                                src={{asset("assets/images/ig.png")}} alt=""></a> -->
                                    <a href="javascript:void(0)" id="download-instagram" class="download-profile"><img
                                                src={{asset("assets/images/download.png")}} alt=""></a>
                                    <a href="https://www.blibli.com/travel/" target="_blank"><img src="{{asset("assets/images/usecode.png")}}" alt=""></a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="area-profile" id="area-profile">
                                    <div class="row">
                                        <div class="col-md-7 col-6">
                                            <img src="{{URL::asset('assets/images/logo-popup.png')}}"
                                                 class="img-fluid logo-modal" alt="">
                                        </div>
                                        <div class="col-md-5 col-6">
                                            <div>
                                                @if(isset($user->image))
                                                    @if (strpos($user->image, 'facebook') !== false)
                                                        <label class="area-photo photo-fb" id="label-area-photo"
                                                               style="background: url('{{$user->image}}') no-repeat"></label>
                                                    @else
                                                        <label class="area-photo photo-fb" id="label-area-photo"
                                                               style="background: url('{{url('uploads/'.$user->image)}}') no-repeat"></label>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <br>
                                            <h4 class="text-viola text-bold">OTHER ATHLETE</h4>
                                            <div class="text-blue text-bold margin-top-20 uppercase">
                                                <h3 class="latoblack">{{isset($user->name)?$user->name:""}}</h3></div>
                                            <p class="text-blue text-bold uppercase country">
                                                <i>{{isset($user->country)?$user->country:"Indonesia"}}</i></p>
                                        </div>
                                    </div>
                                    <div class="row nopadding" style="background: {{ $destination->popup_bgcolor }};">
                                        <div class="col-md-6 col-6 text-center">
                                            <div class="popup-icon-logo">
                                                <img src="{{ url('uploads/'.$destination->icon_thumbnail1) }}"
                                                     alt="">
                                                <p>{{ $destination->category }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-6">
                                            <div class="place" style="background: {{ $destination->popup_btncolor }};">
                                                PLACE: <br>{{ $destination->popup_place }}
                                            </div>
                                            <div class="sitting"
                                                 style="background: {{ $destination->popup_btncolor }};">
                                                SITTING: <br>{{ $destination->popup_sitting }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sosmed-icon sosmed-share d-block d-sm-none text-center" id='sosmed-share-mobile'
                                     data-html2canvas-ignore="true">
                                    <a href="javascript:void(0)" onclick="shareFB()"><img
                                                src={{asset("assets/images/fb.png")}} alt=""></a>
                                    <!-- <a href="https://twitter.com/intent/tweet?text=https://www.othergames2018.com"><img
                                                src={{asset("assets/images/tw.png")}} alt=""></a>
                                    <a href="javascript:void(0)" class="download-instagram"><img
                                                src={{asset("assets/images/ig.png")}} alt=""></a> -->
                                    <a href="javascript:void(0)" class="download-instagram"><img
                                                src={{asset("assets/images/download.png")}} alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none; background: url({{ url('uploads/'.$destination->popup_bgig) }}) no-repeat; background-size: cover; width: 800px;"
         id="download-ig">
        <div class="area-profile-download-ig">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{URL::asset('assets/images/logo-popup.png')}}" class="img-fluid logo-modal" alt="">
                </div>
                <div class="col-md-6">
                    <div>
                        @if(isset($user->image))
                            @if (strpos($user->image, 'facebook') !== false)
                                <label class="area-photo photo-fb" id="label-area-photo"
                                       for="photoProfile"
                                       style="background: url('{{$user->image}}') no-repeat"></label>
                            @else
                                <label class="area-photo photo-fb" id="label-area-photo"
                                       for="photoProfile"
                                       style="background: url('{{url('uploads/'.$user->image)}}') no-repeat"></label>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <br>
                    <h4 class="text-viola text-bold">OTHER ATHLETE</h4>
                    <div class="text-blue text-bold margin-top-20 uppercase">
                        <h3 class="latoblack">{{isset($user->name)?$user->name:""}}</h3></div>
                    <p class="text-blue text-bold uppercase"><i>{{isset($user->country)?$user->country:"Indonesia"}}</i>
                    </p>
                </div>
            </div>
            <div class="row nopadding" style="background: {{ $destination->popup_bgcolor }};">
                <div class="col-md-6 text-center">
                    <div class="popup-icon-logo">
                        <img src="{{ url('uploads/'.$destination->icon_thumbnail1) }}"
                             alt="">
                        <p>{{ $destination->category }}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="place" style="background: {{ $destination->popup_btncolor }};">
                        PLACE: <br>{{ $destination->popup_place }}
                    </div>
                    <div class="sitting" style="background: {{ $destination->popup_btncolor }};">
                        SITTING: <br>{{ $destination->popup_sitting }}
                    </div>
                </div>
            </div>
        </div>
    </div>

<div id="loading" style="display: none;">
  <div class="container">
      <div class="row">
          <div class="col-lg-12 text-center">
              <div class="loader"><img src={{asset("assets/images/loader.gif")}}></div>
          </div>
      </div>
  </div>
</div>

@endsection
@push('styles')
<link rel="stylesheet" href={{asset('assets/vendor/slick/slick.css')}}>
<link rel="stylesheet" href={{asset('assets/vendor/slick/slick-theme.css')}}>
@endpush
@push('scripts')
<script src={{asset('assets/vendor/slick/slick.min.js')}}></script>
<script src={{asset('assets/vendor/dom-to-image/dom-to-image.min.js')}}></script>
<script src={{asset('assets/vendor/html2canvas/html2canvas.js')}}></script>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '282672325645291',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v3.1'
        });
    };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        var path_temp_fb = null;

        $(function () {

            var sites = {!! json_encode($isShared)!!};
            if (sites == 1) {
                $('#hidden_content').show();
                $('#modalRegister').attr('data-target', '#after-register');
            }
            var showpop = {!! json_encode(Session::get('showpop'))!!};
            if (showpop == '1') {
              $('#registerModal').modal('show');
            }

            $('#modalRegister').click(function () {
              shareSosmed();
            });

            function shareSosmed(){
              $('#sosmed-share-desktop').removeClass('d-none d-md-block')
              $('#sosmed-share-mobile').removeClass('sosmed-share d-block d-sm-none text-center')
              $('#sosmed-share-desktop').hide();
              $('#sosmed-share-mobile').hide();
              $('#after-close-button').hide();
              var img = domtoimage.toJpeg(document.getElementById('full-modal-profile'),
                  {
                      quality: 0.95,
                      bgcolor: '#FFFFFF',
                  })
                  .then(ajaxImage.bind(null, null));
            }


            var showpopshare = {!! json_encode(Session::get('showpopshare'))!!};
            if (showpopshare == '1') {
                $('#after-register').modal('show');
                shareSosmed();
            }
        });

        function ajaxImage(fb, data) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '{{route('share.fb.upload')}}',
                data: {
                    image: data,
                },
                success: function (data) {
                    console.log("success upload image crop facebook");
                    path_temp_fb = data;
                    $('#sosmed-share-desktop').addClass('d-none d-md-block')
                    $('#sosmed-share-mobile').addClass('sosmed-share d-block d-sm-none text-center')
                    $('#sosmed-share-desktop').show('slow');
                    $('#sosmed-share-mobile').show('slow');
                    $('#after-close-button').show();
                }
            });

        }

        function shareFB() {
            FB.ui({
                method: 'share_open_graph',
                action_type: 'og.likes',
                action_properties: JSON.stringify({
                    object: {
                        'og:url': 'https://othergames2018.com',
                        'og:title': 'Othergames2018.com',
                        'og:description': 'Support me as an other-athlete in #OtherGames2018. I will do my best to have fun for our country. You can be an other-athlete and travel with special promo from Blibli Travel. Visit OtherGames2018.com',
                        'og:image': '{{url('uploads/')}}'.concat('/').concat(path_temp_fb.path),
                        'og:image:width': '1200',
                        'og:image:height': '688',
                        'og:image:type': 'image/jpeg'
                    },
                })
            }, function (response) {
                if (!response.error_message) {
                    window.location = '{{route('sharefacebook',$destination->slug)}}';
                } else {
                    alert('Error while posting.');
                }
            });
        }

        $('.download-instagram').click(function () {
            $('#download-ig').show();
            html2canvas(document.querySelector("#download-ig"), {
                async: false,
                removeContainer: false,
                allowTaint: true,
            }).then(canvas => {
                saveAs(canvas.toDataURL(), '{{isset($user->name)?$user->name:""}}-instagram.jpeg');
                $('#download-ig').hide();
            });
        });

        $('.download-profile').click(function () {
            html2canvas(document.querySelector("#full-modal-profile"), {
                async: false,
                removeContainer: false,
                allowTaint: true,
            }).then(canvas => {
                saveAs(canvas.toDataURL(), '{{isset($user->name)?$user->name:""}}-profile.jpeg');
            });
        });

        function submitProfile() {
            $('#loading').show();
            $('#form_profile').submit()
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#label-area-photo').css('background', 'url(' + e.target.result + ')');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function saveAs(uri, filename) {
            var link = document.createElement('a');
            if (typeof link.download === 'string') {
                link.href = uri;
                link.download = filename;

                //Firefox requires the link to be in the body
                document.body.appendChild(link);

                //simulate click
                link.click();

                //remove the link when done
                document.body.removeChild(link);
            } else {
                window.open(uri);
            }
        }
    </script>
@endpush
