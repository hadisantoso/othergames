<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Other Games 2018 | Wonderful Indonesia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
    <meta name="description" content="Hosted in 10 wonderful places around Indonesia, Other Games 2018 is the official ‘other’ sport event after Asian Games 2018. Now you can be an ‘athlete’ in more leisurely sports like snorkeling, fishing , island hopping, amongst others that you won’t find in the games.">
    <meta name="robots" content="index, follow" />
    <link rel="canonical" href="https://othergames2018.com/" />

    <link rel="stylesheet" href={{asset('assets/vendor/bootstrap-4.0.0-dist/css/bootstrap.min.css')}}>
    <link rel="stylesheet"
          href={{asset('assets/vendor/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.min.css')}}>
    <link rel="stylesheet" href={{asset('assets/css/imagehover.min.css')}}>
    <link rel="stylesheet" href={{asset('assets/css/animate.css')}}>
    <link rel="stylesheet" href={{asset('assets/css/customs.css')}}>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-57854559-4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-57854559-4');
    </script>

    @stack('styles')
</head>
<body>
@include('layouts.header-meta')
<div id="content">
    @include('layouts.menu')
    @yield('content')
</div>

@include('layouts.footer-meta')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>
@stack('scripts')
</body>
</html>
