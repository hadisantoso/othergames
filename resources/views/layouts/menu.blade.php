<div id="wrapper" class="">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <div class="text-right">
                    <a class="navbar-brand" id="menu-toggle-close"><i class="fa fa-times"></i></a>
                </div>
            </li>
            <li>
                <a href="{{ url('/') }}" class="smooth"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> HOME</a>
            </li>
            <!-- <li>
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span><font color="#337AB7">
                    -----------</font>
            </li> -->
            <!-- @foreach($menu_destinations as $menu_destination)
                <li>
                    <a href="{{route('destination',$menu_destination->slug)}}" class="text-capitalize">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                        {{$menu_destination->title}}</a>
                </li>
            @endforeach -->
            <!-- <li>
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span><font color="#337AB7">
                    -----------</font>
            </li> -->
            <li>
                <a href="#destination-title" class="smooth"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> THE OTHER GAMES</a>
            </li>
            <li>
                <a href="https://www.Indonesia.travel" target="_blank"><span class="glyphicon glyphicon-home"
                                                               aria-hidden="true"></span> VISIT INDONESIA</a>
            </li>
            <!-- <li>
                <a href="{{route('syarat.ketentuan')}}"><span class="glyphicon glyphicon-home"
                                                              aria-hidden="true"></span> SYARAT & KETENTUAN</a>
            </li> -->
           
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
</div>
