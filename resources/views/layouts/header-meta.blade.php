<header>
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-9"></div>
                <div class="col-md-3">
                    <!-- <div class="sosmed">
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-instagram"></i></a>
                        <a href=""><i class="fa fa-youtube-play"></i></a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-rainbow-topbar"></div>
    <div class="area-menu">
        <div class="container-fluid desktop">
            <div class="row">
                <div class="col-md-6 col-9">
                    <a href="{{ url('/') }}"><img src={{asset('assets/images/logo.png')}} class="logo-other-games img-fluid" alt=""></a>
                </div>
                <div class="col-md-5 d-none d-sm-block">
                    <div class="text-right"><img src="{{asset('assets/images/wonderful-indonesia.png')}}" class="img-fluid wi-img" alt=""></div>
                </div>
                <div class="col-md-1 col-3">
                    <div class="text-center nav-hamburger"><a class="navbar-brand" id="menu-toggle-desktop"><i class="fa fa-navicon"></i></a></div>
                </div>
            </div>
        </div>
        <div class="container-fluid mobile">
            <div class="row">
                <div class="col-5">
                    <a href="{{ url('/') }}"><img src={{asset('assets/images/logo.png')}} class="logo-other-games" alt=""></a>
                </div>
                <div class="col-5">
                    <div class="text-right"><img src="{{asset('assets/images/wonderful-indonesia.png')}}" class="img-fluid wi-img" alt=""></div>
                </div>
                <div class="col-2">
                    <div class="text-center nav-hamburger"><a class="navbar-brand" id="menu-toggle-mobile"><i class="fa fa-navicon"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</header>