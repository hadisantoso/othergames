<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Landing Page</title>
    <link rel="stylesheet" href={{asset('/vendor/bootstrap-4.0.0-dist/css/bootstrap.min.css')}}>
    <link rel="stylesheet" href={{asset('/vendor/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.min.css')}}>
    <link rel="stylesheet" href={{asset('/css/imagehover.min.css')}}>
    <link rel="stylesheet" href={{asset('/css/custom.css')}}>
    <link rel="stylesheet" href={{asset('/css/animate.css')}}>
</head>
<body>


    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Launch demo modal
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-custom modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="assets/images/close.png" alt=""></span>
                    </button>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="heading-pop margin-top-145">
                                    Lorem Ipsum<br>
                                    Dolor Other Games
                                </h1>

                                <div class="color-white">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad aliquam consequuntur cupiditate, ex exercitationem fugit natus nihil nulla possimus quod quos tempora. Delectus explicabo, non quas reiciendis voluptas voluptatibus?
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="area-profile">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <img src="assets/images/logo.svg" alt="">
                                            <span class="text-num">3.9.18</span>
                                            <div class="hiking-box">
                                                <img src="assets/images/hiking.png" alt="">
                                                <span class="text-destination-pop">HIKING</span>
                                            </div>
                                            <input type="text" class="form-control your-name margin-bottom-60" placeholder="Your Country">
                                        </div>
                                        <div class="col-md-6">
                                            <div>
                                                <label class="area-photo" for="photoProfile">Upload Your Photo Here!</label>
                                                <input type="file"  id="photoProfile">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control your-name" placeholder="Insert Your Name">
                                            <p class="text-viola text-bold">Other Athlete</p>

                                            <div class="place margin-top-35">
                                                Place : Mt. Bromo
                                            </div>
                                            <div class="sitting">
                                                Sitting : Before Sunrise
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <img src="assets/images/line.jpg" class="img-full" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<footer>

</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
