@extends('layouts.layout')
@section('content')
    <!-- <link rel="stylesheet" href="http://getbootstrap.com.vn/examples/equal-height-columns/equal-height-columns.css"> -->
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-banner-text animated slideInRight">
                        <img src="{{asset("assets/images/text-banner.png")}}" alt="" class="area-banner-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row margin-top-90">
            <div class="col-md-6">
                <!-- <h3 class="title-sec">Other Games 2018</h3>
                <div class="bottom-rainbow">
                </div> -->
                <div class="text-right margin-top-60 margin-bottom-60 d-block d-sm-none">
                    <img class="img-other-games img-fluid" src={{asset("assets/images/other-games.png")}} alt="">
                </div>
                <div class="short-content">
                    <div class="one-p">Hosted in nine wonderful places around Indonesia, Other Games 2018 is the official "other" sport event after Asian Games 2018.</div> 
                    <br><p>Now you can be an “athlete” in leisure sports like snorkeling, lake fishing, island hopping, and other sports that you won’t find in the games.</p>
                    <br><p>Go from gold medals to golden sunsets, training to tanning, racing to relaxing, as we invite you to discover the other beautiful parts of Indonesia at your own pace.</p>
                </div>

                <!-- <div class="margin-top-60">
                    <a href="#destination-title" class="col-sm-12 smooth">
                        <img src={{asset("assets/images/btn-be-an-other-athelete-now.png")}}
                         class="btn-be-an-other-athelete-now"       alt="">
                    </a>
                </div> -->
            </div>
            <div class="col-md-6">
                <div class="text-right margin-top-100 d-none d-lg-block d-xl-block">
                    <img class="img-fluid" src={{asset("assets/images/other-games.png")}} alt="">
                </div>
            </div>
        </div>
        <div class="row rainbow">
            <div class="col-md-12">
                <div class="bottom-rainbow-long"></div>
            </div>
        </div>

        <h3 class="title-sec" id="destination-title">The Other Games</h3>
        <div class="row" id="destination">
            @foreach($destinations as $key=>$destination)
                <div class="col-md-6">
                    <a href="{{route('destination',$destination->slug)}}">
                        <figure class="imghvr-slide-up width-full bg-overlay">
                            <img src="{{ url('uploads/'.$destination->thumbnail) }}"
                                 class="img-fluid width-full">
                            <figcaption class="desktop">
                                <div>
                                    <h4 class="upppercase">{{$destination->thumbnail_title}}</h4><br>
                                    <p>{{$destination->thumbnail_desc}}</p><br>
                                    <img src={{asset("assets/images/read-more.png")}} alt="" class="read-more">
                                </div>
                            </figcaption>
                            <figcaption class="mobile">
                                <div>
                                    <h4 class="upppercase">{{$destination->thumbnail_title}}</h4>
                                    <p>{{$destination->thumbnail_desc}}</p>
                                    <img src={{asset("assets/images/read-more.png")}} alt="" class="read-more">
                                </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="bg-video margin-top-130">
        <div class="container">
            <div class="col-md-12">
                <div class="center-video">
                    <div id="video-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <!-- @foreach ($videos as $video)
                                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                    <video width="100%" class="video-custom" controls
                                           poster={{ url('uploads/'.$video->poster) }}>
                                        <source src={{ url('uploads/'.$video->video) }} type="video/mp4">
                                        Your browser does not support HTML5 video.
                                    </video>
                                </div>
                            @endforeach -->
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RZHN4KjUhlY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <a class="carousel-control-prev" href="#video-carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#video-carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <!-- <div class="sosmed-video">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-instagram"></i>
                        <i class="fa fa-youtube-play"></i>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container-full mar-bot-100-xs">
        <div class="row row-eq-height">
            <div class="col-md-6 bg-orange">
                <a href="http://instagram.com/explore/tags/othergames2018" target="_blank">
                    <div class="bg-orange-val">
                        <div class="hashtag">
                            #OTHERSGAMES2018
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 bg-blue">
                <a href="https://www.Indonesia.travel" target="_blank">
                    <div class="bg-blue-val">
                        <div class="hashtag">
                            VISIT INDONESIA
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div> -->
    <div class="container">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <div class="text-center padding-40" id="logo-bottom"><img src="{{asset("assets/images/wonderful-indonesia.png")}}" alt="" class="img-fluid logo-bottom"></div>
            </div>
            <div class="col-sm-3">
                <div class="text-center padding-40" id="logo-bottom"><img src="{{asset("assets/images/ticket-partner-blibli.png")}}" alt="" class="img-fluid logo-bottom"></div>
            </div>
        </div>
    </div>
@endsection
