<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href='{{ backpack_url('homeslider') }}'><i class='fa fa-tag'></i> <span>Home Sliders</span></a></li>
<li><a href='{{ backpack_url('destination') }}'><i class='fa fa-tag'></i> <span>Destinations</span></a></li>
<li><a href='{{ backpack_url('customer') }}'><i class='fa fa-tag'></i> <span>Customers</span></a></li>
<li><a href='{{ backpack_url('syaratketentuan') }}'><i class='fa fa-tag'></i> <span>Syarat dan Ketentuan</span></a></li>
<li><a href='{{ backpack_url('kebijakanprivasi') }}'><i class='fa fa-tag'></i> <span>Kebijakan Privasi</span></a></li>

<li><a href='{{ backpack_url('homevideoslider') }}'><i class='fa fa-tag'></i> <span>Videos</span></a></li>

<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
