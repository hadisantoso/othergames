<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('destinations', function (Blueprint $table) {
          $table->string('category');
          $table->string('popup_title');
          $table->string('popup_desc');
          $table->string('popup_place');
          $table->string('popup_sitting');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('destinations', function (Blueprint $table) {
          $table->dropColumn('category');
          $table->dropColumn('popup_title');
          $table->dropColumn('popup_desc');
          $table->dropColumn('popup_place');
          $table->dropColumn('popup_sitting');
        });
    }
}
