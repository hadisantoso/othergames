<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('icon_thumbnail1')->nullable();
            $table->string('icon_thumbnail2')->nullable();
            $table->string('title_detail');
            $table->string('title_detail2')->nullable();
            $table->string('featured_image')->nullable();
            $table->mediumText('sort_desc');
            $table->string('image1')->nullable();
            $table->longText('image1_desc');
            $table->string('image2')->nullable();
            $table->longText('image2_desc');
            $table->string('icon_image_detail')->nullable();
            $table->string('link_ticket')->nullable();
            $table->smallInteger('is_featured')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinations');
    }
}
